package fr.triocode.coincollector.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import fr.triocode.coincollector.data.Coin
import fr.triocode.coincollector.data.Rarity
import fr.triocode.coincollector.utils.Formatteur.Companion.formatCurrency

@Composable
fun Card(
    modifier: Modifier = Modifier,
    elevation: Dp = 1.dp,
    content: @Composable () -> Unit
) {
    val cardShape = RoundedCornerShape(8.dp)
    Surface(
        modifier = modifier,
        shape = cardShape,
        shadowElevation = elevation,
        color = MaterialTheme.colorScheme.surface
    ) {
        content()
    }
}

@Composable
fun CoinItem(coin: Coin, quantity: Int, onClick: () -> Unit) {

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
            .clickable(onClick = onClick),
        elevation = 4.dp
    ) {
        Row (modifier = Modifier.padding(16.dp)){
            Image(
                painter = rememberAsyncImagePainter(model = coin.imagePath),
                contentDescription = "Coin Image",
                modifier = Modifier
                    .size(100.dp)
                    .padding(end = 16.dp)
            )
            Column(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = "Année: ${coin.year}",
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    text = "Rareté: ${coin.rarity.value}",
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    text = "Valeur: ${formatCurrency(coin.value)}",
                    style = MaterialTheme.typography.titleLarge
                )
                Text(
                    text = "Quantité: ${quantity}",
                    style = MaterialTheme.typography.titleLarge
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun CoinItemPreview() {
    CoinItem(
        coin = Coin(1, 2021, Rarity.COMMON, 10.0, null),
        quantity = 25,
        onClick = {}
    )
}
