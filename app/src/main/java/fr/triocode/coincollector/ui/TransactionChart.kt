package fr.triocode.coincollector.ui

import android.graphics.Color
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import fr.triocode.coincollector.data.Coin
import fr.triocode.coincollector.data.CoinTransaction
import fr.triocode.coincollector.data.Rarity
import fr.triocode.coincollector.utils.DateValueFormatter
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@Composable
fun TransactionChart(transactions: List<CoinTransaction>, coin: Coin, modifier: Modifier = Modifier) {
    val context = LocalContext.current
    val chart = remember { LineChart(context) }

    // Grouper les transactions par date
    val groupedTransactions = transactions.groupBy { it.date }.toSortedMap()

    var cumulativeValue = 0.0
    // Créer les entrées pour le graphique avec des valeurs cumulatives
    val entries = groupedTransactions.toSortedMap().values.flatMapIndexed { index, transactionsOnDate ->
        transactionsOnDate.map { transaction ->
            cumulativeValue += transaction.quantity * coin.value
            Entry(index.toFloat(), cumulativeValue.toFloat())
        }
    }

    val dataSet = LineDataSet(entries, "Quantity over time").apply {
        color = Color.BLUE
        valueTextSize = 12f
        mode = LineDataSet.Mode.STEPPED
    }

    val dates = transactions.distinctBy { it.date }.map { it.date }

    chart.data = LineData(dataSet)

    // Configure X axis
    val xAxis = chart.xAxis
    xAxis.position = XAxis.XAxisPosition.BOTTOM
    xAxis.valueFormatter = DateValueFormatter(dates)
    xAxis.setDrawGridLines(false)
    xAxis.granularity = 1f

    // Configure left Y axis
    val yAxisLeft = chart.axisLeft
    yAxisLeft.setDrawGridLines(false)

    // Disable right Y axis
    val yAxisRight = chart.axisRight
    yAxisRight.isEnabled = false

    // Disable description
    chart.description.isEnabled = false

    chart.invalidate() // Refresh the chart

    // Enable scrolling and zooming
    chart.isDragEnabled = true
    chart.setScaleEnabled(true)
    chart.setPinchZoom(true)

    // Set an initial viewport size to enable horizontal scrolling
    chart.setVisibleXRangeMaximum(5f) // Adjust this value as needed

    AndroidView({ chart }, modifier = modifier)
}

@Preview(showBackground = true)
@Composable
fun TransactionChartPreview() {
    val coin = Coin(1, 2020, Rarity.COMMON, 100.0, null)
    val transactions = listOf(
        CoinTransaction(1, 1, 10, Date(124,1,1)),
        CoinTransaction(1, 1, 10, Date(124,1,1)),
        CoinTransaction(2, 1, 15, Date(124, 4, 6)),
        CoinTransaction(3, 1, 30, Date(124, 4, 24))
    )
    TransactionChart(transactions = transactions, coin = coin, modifier = Modifier
        .fillMaxSize()
        .padding(10.dp, 0.dp))
}

@Preview(showBackground = true)
@Composable
fun TransactionChartPreview2() {
    val coin = Coin(1, 2020, Rarity.COMMON, 100.0, null)
    val transactions = listOf(
        CoinTransaction(1, 1, 10, SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).parse("01-02-2024")),
        CoinTransaction(1, 1, 10, Date(124,0,1)),
    )
    TransactionChart(transactions = transactions, coin = coin, modifier = Modifier
        .fillMaxSize()
        .padding(10.dp, 0.dp))
}
