package fr.triocode.coincollector.ui

import android.graphics.Bitmap
import android.icu.util.LocaleData
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddPhotoAlternate
import androidx.compose.material.icons.filled.CameraAlt
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import coil.compose.rememberAsyncImagePainter
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import fr.triocode.coincollector.data.Coin
import fr.triocode.coincollector.data.CoinDatabaseHelper
import fr.triocode.coincollector.data.CoinTransaction
import fr.triocode.coincollector.data.Rarity
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale

@ExperimentalMaterial3Api
@Composable
fun AddCoinScreen(navController: NavHostController) {
    val context = LocalContext.current
    val dbHelper = remember { CoinDatabaseHelper(context) }

    AddCoinForm { coin, quantity, date ->
        val existingCoin = dbHelper.getCoin(coin.year, coin.rarity, coin.value)
        val coinId: Long
        System.out.println(date)

        if (existingCoin != null) {
            // La pièce existe déjà, utilisez son ID
            coinId = existingCoin.id
        } else {
            // La pièce n'existe pas, ajoutez-la et obtenez l'ID généré
            coinId = dbHelper.addCoin(coin)
        }

        // Créer la transaction avec l'ID de la pièce
        val transaction = CoinTransaction(
            id = 0,
            coinId = coinId,
            quantity = quantity,
            date = date
        )

        System.out.println(transaction.date)

        dbHelper.addTransaction(transaction)
        navController.navigate("home") // Navigate back to home after adding the coin and transaction
    }
}

@ExperimentalMaterial3Api
@Composable
fun AddCoinForm(onSubmit: (Coin, Int, Date) -> Unit) {
    val context = LocalContext.current
    var year by remember { mutableStateOf("") }
    var rarity by remember { mutableStateOf(Rarity.COMMON.value) }
    var value by remember { mutableStateOf("") }
    var quantity by remember { mutableStateOf("") }
    val defaultDate = remember { SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date()) }
    var date by remember {
        mutableStateOf(defaultDate)
    }

    var imagePath by remember { mutableStateOf("") }

    val imagePickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        uri?.let {
            val file = File(context.filesDir, "${System.currentTimeMillis()}.jpg")
            val inputStream = context.contentResolver.openInputStream(uri)
            val outputStream = FileOutputStream(file)
            inputStream?.copyTo(outputStream)
            inputStream?.close()
            outputStream.close()
            imagePath = file.absolutePath
        }
    }

    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview()
    ) { bitmap: Bitmap? ->
        bitmap?.let {
            val file = File(context.filesDir, "${System.currentTimeMillis()}.jpg")
            val outputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            outputStream.close()
            imagePath = file.absolutePath
        }
    }

    // État pour gérer l'ouverture/la fermeture du menu déroulant
    var expanded by remember { mutableStateOf(false) }
    var textfieldSize by remember { mutableStateOf(Size.Zero) }
    val datePickerDialogState = rememberMaterialDialogState()

    val icon = if (expanded)
        Icons.Filled.KeyboardArrowUp //it requires androidx.compose.material:material-icons-extended
    else
        Icons.Filled.KeyboardArrowDown


    Column(modifier = Modifier.padding(16.dp)) {
        OutlinedTextField(
            value = year,
            onValueChange = { year = it },
            label = { Text("Year") },
            modifier = Modifier.fillMaxWidth(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        Box(modifier = Modifier.fillMaxWidth()) {
            OutlinedTextField(
                value = rarity,
                onValueChange = { rarity = it },
                modifier = Modifier
                    .fillMaxWidth()
                    .onGloballyPositioned { coordinates ->
                        //This value is used to assign to the DropDown the same width
                        textfieldSize = coordinates.size.toSize()
                    },
                label = { Text("Rarity") },
                trailingIcon = {
                    Icon(icon, "contentDescription",
                        Modifier.clickable { expanded = true })
                }
            )
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                modifier = Modifier
                    .width(with(LocalDensity.current) { textfieldSize.width.toDp() })
            ) {
                Rarity.entries.forEach { rarityOption ->
                    DropdownMenuItem(text = { Text(rarityOption.value) }, onClick = {
                        rarity = rarityOption.value
                        expanded = false
                    })
                }
            }
        }
        OutlinedTextField(
            value = value,
            onValueChange = { value = it },
            label = { Text("Value") },
            modifier = Modifier.fillMaxWidth(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        OutlinedTextField(
            value = quantity,
            onValueChange = { quantity = it },
            label = { Text("Quantity") },
            modifier = Modifier.fillMaxWidth(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )
        OutlinedTextField(
            value = date,
            onValueChange = { date = it },
            label = { Text("Date") },
            readOnly = true,
            modifier = Modifier.fillMaxWidth(),
            trailingIcon = {
                IconButton(onClick = {
                    datePickerDialogState.show()
                }) {
                    Icon(Icons.Filled.DateRange, contentDescription = "Select Date")
                }
            }
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(end = 16.dp)
            ) {
                IconButton(
                    onClick = { imagePickerLauncher.launch("image/*") },
                    modifier = Modifier.size(60.dp)
                ) {
                    Icon(
                        imageVector = Icons.Default.AddPhotoAlternate,
                        contentDescription = "Select Image",
                        tint = Color.Gray,
                        modifier = Modifier.size(40.dp)
                    )
                }
                Text(
                    text = "Select Image",
                    style = MaterialTheme.typography.bodySmall
                )
                Spacer(modifier = Modifier.height(8.dp))
                IconButton(
                    onClick = { cameraLauncher.launch(null) },
                    modifier = Modifier.size(60.dp)
                ) {
                    Icon(
                        imageVector = Icons.Default.CameraAlt,
                        contentDescription = "Take Photo",
                        tint = Color.Gray,
                        modifier = Modifier.size(40.dp)
                    )
                }
                Text(
                    text = "Take Photo",
                    style = MaterialTheme.typography.bodySmall
                )
            }
            Spacer(modifier = Modifier.weight(1f))
            if (!imagePath.isNullOrEmpty()) {
                Image(
                    painter = rememberAsyncImagePainter(model = imagePath),
                    contentDescription = "Coin Image",
                    modifier = Modifier
                        .size(200.dp)
                        .clip(RoundedCornerShape(16.dp))
                        .border(2.dp, Color.Gray, RoundedCornerShape(16.dp))
                        .padding(end = 16.dp)
                )
            }
        }

        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = {
                val newCoin = Coin(
                    id = 0,
                    year = year.toInt(),
                    rarity = Rarity.entries.find { it.value == rarity }!!,
                    value = value.toDouble(),
                    imagePath = imagePath
                )
                System.out.println(date)
                val transactionDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).parse(date)!!
                System.out.println(transactionDate)
                onSubmit(newCoin, quantity.toInt(), transactionDate)
            },
            modifier = Modifier.align(Alignment.End)
        ) {
            Text("Add Coin")
        }
    }
    MaterialDialog(
        dialogState = datePickerDialogState,
        buttons = {
            positiveButton("Ok")
            negativeButton("Cancel")
        }
    ) {
        datepicker (
            locale = Locale.FRANCE,
            onDateChange = {
                date = it.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
            },
            allowedDateValidator = { it.isBefore(LocalDate.now().plusDays(1)) }
        )
    }
}



@ExperimentalMaterial3Api
@Preview(showBackground = true)
@Composable
fun AddCoinScreenPreview() {
    val navController = rememberNavController()
    AddCoinScreen(navController = navController)
}
