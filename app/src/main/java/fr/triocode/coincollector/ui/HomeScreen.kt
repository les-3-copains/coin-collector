package fr.triocode.coincollector.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import fr.triocode.coincollector.data.Coin
import fr.triocode.coincollector.data.CoinDatabaseHelper
import fr.triocode.coincollector.data.CoinTransaction
import fr.triocode.coincollector.ui.theme.CoinCollectorTheme
import java.util.Date

@Composable
fun HomeScreen(navController: NavHostController) {
    val context = LocalContext.current
    val dbHelper = remember { CoinDatabaseHelper(context) }
    val coins by remember { mutableStateOf(dbHelper.getAllCoins()) }
    val totalValuesByDate by remember { mutableStateOf(dbHelper.getTotalValueByDate()) }
    val transactions by remember { mutableStateOf(dbHelper.getAllCoinTransactions()) }

    CoinList(navController = navController, coins = coins, transactions = transactions, totalValuesByDate = totalValuesByDate)
}

@Composable
fun CoinList(navController: NavHostController, coins: List<Coin>, transactions: List<CoinTransaction>, totalValuesByDate: List<Pair<Date, Double>>) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            if (coins.isEmpty()) {
                Text(
                    text = "No coins available",
                    style = MaterialTheme.typography.titleMedium,
                    modifier = Modifier.padding(vertical = 20.dp)
                )
            } else {
                LazyColumn(
                    modifier = Modifier.weight(0.75f) // Utilisation de 75% de l'espace disponible
                ) {
                    items(coins) { coin ->
                        var sumQuantity = 0;
                        transactions.filter { it.coinId == coin.id }.forEach { sumQuantity += it.quantity }
                        CoinItem(coin = coin, quantity = sumQuantity, onClick = { navController.navigate("coinDetail/${coin.id}") })
                    }
                }
            }
            TransactionTotalValueChart(
                transactions = transactions,
                coins = coins,
                modifier = Modifier
                    .weight(0.25f) // Utilisation de 25% de l'espace disponible
                    .fillMaxWidth()
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HomeScreenPreview() {
    val navController = rememberNavController()
    CoinCollectorTheme {
        HomeScreen(navController = navController)
    }
}

