package fr.triocode.coincollector.ui

import android.graphics.Bitmap
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddPhotoAlternate
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.CameraAlt
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import fr.triocode.coincollector.data.CoinDatabaseHelper
import fr.triocode.coincollector.utils.Formatteur
import java.io.File
import java.io.FileOutputStream

@ExperimentalMaterial3Api
@Composable
fun CoinDetailScreen(coinId: Long, navController: NavHostController) {
    val context = LocalContext.current
    val dbHelper = remember { CoinDatabaseHelper(context) }
    val coin = dbHelper.getCoinById(coinId)
    val transactions = dbHelper.getTransactionsByCoinId(coinId)
    var quantity = 0
    transactions.forEach { quantity += it.quantity }

    var imagePath by remember { mutableStateOf(coin?.imagePath) }

    val imagePickerLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        uri?.let {
            val file = File(context.filesDir, "${System.currentTimeMillis()}.jpg")
            val inputStream = context.contentResolver.openInputStream(uri)
            val outputStream = FileOutputStream(file)
            inputStream?.copyTo(outputStream)
            inputStream?.close()
            outputStream.close()
            imagePath = file.absolutePath
            dbHelper.addPicture(coinId, imagePath!!)
            navController.navigate("coinDetail/${coinId}")
        }
    }

    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview()
    ) { bitmap: Bitmap? ->
        bitmap?.let {
            val file = File(context.filesDir, "${System.currentTimeMillis()}.jpg")
            val outputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            outputStream.close()
            imagePath = file.absolutePath
            dbHelper.addPicture(coinId, imagePath!!)
            navController.navigate("coinDetail/${coinId}")
        }
    }

    coin?.let {
        Scaffold(
            topBar = {
                CenterAlignedTopAppBar(
                    title = { Text("Coin Collector") },
                    modifier = Modifier.background(MaterialTheme.colorScheme.background),
                    navigationIcon = {
                        IconButton(onClick = { navController.navigate("home") }) {
                            Icon(Icons.Default.ArrowBack, contentDescription = "Back")
                        }
                    },
                    actions = {
                        FloatingActionButton(
                            onClick = { navController.navigate("add_transaction/${coin.id}") },
                            modifier = Modifier.padding(start = 16.dp, top = 8.dp)
                        ) {
                            Icon(Icons.Rounded.Add, contentDescription = "Add Transaction")
                        }
                    }
                )
            },
            content =  {
                Column(modifier = Modifier.padding(it)) {
                    Row(modifier = Modifier.padding(16.dp)) {
                        if (imagePath != null) {
                            Image(
                                painter = rememberAsyncImagePainter(model = imagePath),
                                contentDescription = "Coin Image",
                                modifier = Modifier
                                    .size(100.dp)
                                    .padding(end = 16.dp)
                            )
                        } else {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                modifier = Modifier.padding(end = 16.dp)
                            ) {
                                IconButton(
                                    onClick = { imagePickerLauncher.launch("image/*") },
                                    modifier = Modifier.size(60.dp)
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.AddPhotoAlternate,
                                        contentDescription = "Select Image",
                                        tint = Color.Gray,
                                        modifier = Modifier.size(40.dp)
                                    )
                                }
                                Text(
                                    text = "Select Image",
                                    style = MaterialTheme.typography.bodySmall
                                )
                                Spacer(modifier = Modifier.height(8.dp))
                                IconButton(
                                    onClick = { cameraLauncher.launch(null) },
                                    modifier = Modifier.size(60.dp)
                                ) {
                                    Icon(
                                        imageVector = Icons.Default.CameraAlt,
                                        contentDescription = "Take Photo",
                                        tint = Color.Gray,
                                        modifier = Modifier.size(40.dp)
                                    )
                                }
                                Text(
                                    text = "Take Photo",
                                    style = MaterialTheme.typography.bodySmall
                                )
                            }
                        }

                        Column(modifier = Modifier.padding(8.dp)) {
                            Text(
                                text = "Année: ${coin.year}",
                                style = MaterialTheme.typography.titleLarge
                            )
                            Text(
                                text = "Rareté: ${coin.rarity.value}",
                                style = MaterialTheme.typography.titleLarge
                            )
                            Text(
                                text = "Valeur: ${Formatteur.formatCurrency(coin.value)}",
                                style = MaterialTheme.typography.titleLarge
                            )
                            Text(
                                text = "Quantité: $quantity",
                                style = MaterialTheme.typography.titleLarge
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(16.dp))

                    TransactionChart(
                        transactions = transactions,
                        coin = coin,
                        modifier = Modifier.fillMaxSize().padding(10.dp, 0.dp)
                    )
                }
            }
        )
    }
}