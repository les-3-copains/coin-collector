package fr.triocode.coincollector.ui

import android.graphics.Color
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import fr.triocode.coincollector.data.Coin
import fr.triocode.coincollector.data.CoinTransaction
import fr.triocode.coincollector.data.Rarity
import fr.triocode.coincollector.utils.DateValueFormatter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@Composable
fun TransactionTotalValueChart(transactions: List<CoinTransaction>, coins: List<Coin>, modifier: Modifier = Modifier) {
    val context = LocalContext.current
    val chart = remember { LineChart(context) }

    // Grouper les transactions par date
    val groupedTransactions = transactions.groupBy {
        it.date
    }

    var cumulativeValue = 0.0
    // Créer les entrées pour le graphique avec des valeurs cumulatives
    val entries = groupedTransactions.entries.mapIndexed { index, entry ->
        val transactionsOnDate = entry.value
        transactionsOnDate.forEach { transaction ->
            cumulativeValue += transaction.quantity * coins.find { it.id == transaction.coinId }?.value!!
        }
        Entry(index.toFloat(), cumulativeValue.toFloat())
    }

    val dataSet = LineDataSet(entries, "Quantity over time").apply {
        color = Color.GREEN
        valueTextSize = 12f
        mode = LineDataSet.Mode.STEPPED
    }

    val dates = transactions.distinctBy { it.date }.map { it.date }

    chart.data = LineData(dataSet)

    // Configure X axis
    val xAxis = chart.xAxis
    xAxis.position = XAxis.XAxisPosition.BOTTOM
    xAxis.valueFormatter = DateValueFormatter(dates)
    xAxis.setDrawGridLines(false)
    xAxis.granularity = 1f

    // Configure left Y axis
    val yAxisLeft = chart.axisLeft
    yAxisLeft.setDrawGridLines(false)

    // Disable right Y axis
    val yAxisRight = chart.axisRight
    yAxisRight.isEnabled = false

    // Disable description
    chart.description.isEnabled = false

    chart.invalidate() // Refresh the chart

    // Enable scrolling and zooming
    chart.isDragEnabled = true
    chart.setScaleEnabled(true)
    chart.setPinchZoom(true)

    // Set an initial viewport size to enable horizontal scrolling
    chart.setVisibleXRangeMaximum(5f) // Adjust this value as needed

    AndroidView({ chart }, modifier = modifier)
}

@Preview(showBackground = true)
@Composable
fun TransactionTotalValueChartPreview() {
    val coins = listOf(
        Coin(1, 2020, Rarity.COMMON, 10.0, null),
        Coin(2, 2020, Rarity.UNCOMMON, 50.0, null),
        Coin(3, 2020, Rarity.LEGENDARY, 900.0, null)
    )
    val transactions = listOf(
        CoinTransaction(1, 1, 10, Date(124,1,1)),
        CoinTransaction(1, 1, 10, Date(124,1,1)),
        CoinTransaction(2, 2, 15, Date(124, 4, 6)),
        CoinTransaction(3, 2, 30, Date(124, 4, 24))
    )
    TransactionTotalValueChart(transactions = transactions, coins = coins, modifier = Modifier.fillMaxSize().padding(10.dp, 0.dp))
}

@Preview(showBackground = true)
@Composable
fun TransactionTotalValueChartPreview2() {
    val coins = listOf(
        Coin(1, 2020, Rarity.COMMON, 10.0, null),
    )
    val transactions = listOf(
        CoinTransaction(1, 1, 10, Date(124,1,1)),
        CoinTransaction(1, 1, 10, Date(123,1,1)),
    )
    TransactionTotalValueChart(transactions = transactions, coins = coins, modifier = Modifier.fillMaxSize().padding(10.dp, 0.dp))
}