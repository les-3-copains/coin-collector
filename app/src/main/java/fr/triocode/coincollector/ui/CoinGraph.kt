package fr.triocode.coincollector.ui

import android.content.Context
import android.graphics.Color
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import fr.triocode.coincollector.data.Coin
import fr.triocode.coincollector.data.Rarity
import fr.triocode.coincollector.ui.theme.CoinCollectorTheme

@Composable
fun CoinValueChart(coins: List<Coin>, modifier: Modifier = Modifier) {
    val context = LocalContext.current
    AndroidView(
        factory = { ctx ->
            createLineChart(ctx, coins)
        },
        modifier = modifier
    )
}

private fun createLineChart(context: Context, coins: List<Coin>): LineChart {
    val lineChart = LineChart(context)

    val entries = coins.mapIndexed { index, coin ->
        Entry(index.toFloat(), (coin.value).toFloat())
    }

    val lineDataSet = LineDataSet(entries, "Total Value").apply {
        color = Color.BLUE
        valueTextColor = Color.BLACK
        valueTextSize = 4f
        setDrawFilled(true)
    }

    val lineData = LineData(lineDataSet)
    lineChart.data = lineData

    lineChart.xAxis.apply {
        position = XAxis.XAxisPosition.BOTTOM
        valueFormatter = XAxisValueFormatter(coins)
        granularity = 1f
    }

    lineChart.axisLeft.apply {
        axisMinimum = 0f
        valueFormatter = YAxisValueFormatter()
    }

    lineChart.axisRight.isEnabled = false
    lineChart.description.isEnabled = false
    lineChart.invalidate() // Refresh the chart

    return lineChart
}

class XAxisValueFormatter(private val coins: List<Coin>) : ValueFormatter() {
    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
        val index = value.toInt()
        return if (index in coins.indices) {
            coins[index].year.toString()
        } else {
            ""
        }
    }
}

class YAxisValueFormatter : ValueFormatter() {
    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
        return "$${value.toInt()}"
    }

    override fun getFormattedValue(value: Float): String {
        return "$${value.toInt()}"
    }

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return "$${value.toInt()}"
    }
}

@Preview(showBackground = true)
@Composable
fun CoinGraphPreview() {
    val sampleCoins = listOf(
        Coin(1, 1990, Rarity.COMMON, 50.0, null),
        Coin(2, 1980, Rarity.RARE, 150.0, null),
        Coin(3, 2000, Rarity.VERY_RARE, 500.0, null),
        Coin(4, 2010, Rarity.UNCOMMON, 75.0, null),
        Coin(5, 2020, Rarity.LEGENDARY, 1000.0, null)
    )
    CoinCollectorTheme {
        // Replace with your actual implementation
        CoinValueChart(coins = sampleCoins, modifier = Modifier.fillMaxSize())
    }
}
