package fr.triocode.coincollector.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import java.text.SimpleDateFormat
import java.util.Locale

@ExperimentalMaterial3Api
@Composable
fun MainScreen() {
    val navController = rememberNavController()
    val currentBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = currentBackStackEntry?.destination?.route

    Scaffold(
        topBar = {
            if (currentRoute == "home") {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(56.dp)
                        .background(MaterialTheme.colorScheme.background)
                        .padding(horizontal = 16.dp)
                ) {
                    Text(
                        text = "Coin Collector",
                        style = MaterialTheme.typography.titleLarge,
                        modifier = Modifier.weight(1f)
                    )
                    FloatingActionButton(
                        onClick = { navController.navigate("add_coin") },
                        modifier = Modifier.padding(start = 16.dp, top = 8.dp)
                    ) {
                        Icon(Icons.Rounded.Add, contentDescription = "Add Coin")
                    }
                }
            }
        }
    ) { paddingValues ->
        Box(modifier = Modifier.fillMaxSize().padding(paddingValues)) {
            NavHost(navController, startDestination = "home", modifier = Modifier.fillMaxSize()) {
                composable("home") { HomeScreen(navController) }
                composable("add_coin") { AddCoinScreen(navController) }
                composable("coinDetail/{coinId}") { backStackEntry ->
                    val coinId = backStackEntry.arguments?.getString("coinId")?.toLong() ?: return@composable
                    CoinDetailScreen(coinId, navController)
                }
                composable("add_transaction/{coinId}") {
                    val coinId = it.arguments?.getString("coinId")?.toLong() ?: return@composable
                    AddTransactionScreen(coinId, navController)
                }
            }
        }
    }
}