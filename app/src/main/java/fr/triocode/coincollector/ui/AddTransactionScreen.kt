package fr.triocode.coincollector.ui

import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.vanpra.composematerialdialogs.MaterialDialog
import com.vanpra.composematerialdialogs.datetime.date.datepicker
import com.vanpra.composematerialdialogs.rememberMaterialDialogState
import fr.triocode.coincollector.data.Coin
import fr.triocode.coincollector.data.CoinDatabaseHelper
import fr.triocode.coincollector.data.CoinTransaction
import fr.triocode.coincollector.data.Rarity
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.Date
import java.util.Locale

@ExperimentalMaterial3Api
@Composable
fun AddTransactionScreen(coinId: Long, navController: NavHostController) {
    val context = LocalContext.current
    val dbHelper = remember { CoinDatabaseHelper(context) }

    val coin = dbHelper.getCoinById(coinId)!!

    AddTransactionForm(coin = coin) { quantity, date ->
        // Créer la transaction avec l'ID de la pièce
        val transaction = CoinTransaction(
            id = 0,
            coinId = coinId,
            quantity = quantity,
            date = date
        )

        dbHelper.addTransaction(transaction)
        navController.navigate("coinDetail/${coinId}")
    }
}

@ExperimentalMaterial3Api
@Composable
fun AddTransactionForm(coin: Coin, onSubmit: (Int, Date) -> Unit) {
    var quantity by remember { mutableStateOf("") }
    val defaultDate = remember { SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(Date()) }
    var date by remember {
        mutableStateOf(defaultDate)
    }
    val datePickerDialogState = rememberMaterialDialogState()

    Column(modifier = Modifier.padding(16.dp)) {
        // Year field with TextField (non-editable)
        TextField(
            value = coin.year.toString(),
            onValueChange = {}, // Empty onValueChange to make it non-editable
            label = { Text("Year") },
            readOnly = true,
            modifier = Modifier.fillMaxWidth()
        )

        // Rarity field with TextField (non-editable)
        TextField(
            value = coin.rarity.value,
            onValueChange = {}, // Empty onValueChange to make it non-editable
            label = { Text("Rarity") },
            readOnly = true,
            modifier = Modifier.fillMaxWidth()
        )

        // Value field with TextField (non-editable)
        TextField(
            value = coin.value.toString(),
            onValueChange = {}, // Empty onValueChange to make it non-editable
            label = { Text("Value") },
            readOnly = true,
            modifier = Modifier.fillMaxWidth()
        )

        // Quantity field with OutlinedTextField (editable)
        OutlinedTextField(
            value = quantity,
            onValueChange = { quantity = it },
            label = { Text("Quantity") },
            modifier = Modifier.fillMaxWidth(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )

        // Date field with OutlinedTextField (editable)
        OutlinedTextField(
            value = date,
            onValueChange = { date = it },
            label = { Text("Date") },
            readOnly = true,
            modifier = Modifier.fillMaxWidth(),
            trailingIcon = {
                IconButton(onClick = {
                    datePickerDialogState.show()
                }) {
                    Icon(Icons.Filled.DateRange, contentDescription = "Select Date")
                }
            }
        )

        Spacer(modifier = Modifier.height(16.dp))
        Button(
            onClick = {
                val transactionDate = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).parse(date)!!
                onSubmit(quantity.toInt(), transactionDate)
            },
            modifier = Modifier.align(Alignment.End)
        ) {
            Text("Add Coin")
        }
    }
    MaterialDialog(
        dialogState = datePickerDialogState,
        buttons = {
            positiveButton("Ok")
            negativeButton("Cancel")
        }
    ) {
        datepicker { selectedDate ->
            date = selectedDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
        }
    }
}
