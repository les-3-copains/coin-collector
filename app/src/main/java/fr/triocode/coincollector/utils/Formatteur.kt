package fr.triocode.coincollector.utils

import java.text.NumberFormat
import java.util.Locale

class Formatteur {
    companion object {
        fun formatCurrency(value: Double): String {
            val format = NumberFormat.getCurrencyInstance(Locale.FRANCE)
            return format.format(value)
        }
    }

}