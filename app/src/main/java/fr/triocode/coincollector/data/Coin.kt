package fr.triocode.coincollector.data

import java.io.Serializable

data class Coin(
    val id: Long,
    val year: Int,
    val rarity: Rarity,
    val value: Double,
    val imagePath: String? // New field for storing the image path
) : Serializable