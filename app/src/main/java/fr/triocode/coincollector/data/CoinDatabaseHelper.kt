package fr.triocode.coincollector.data

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class CoinDatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 2
        private const val DATABASE_NAME = "coins.db"
        private const val TABLE_COIN = "coin"
        private const val COLUMN_COIN_ID = "id"
        private const val COLUMN_YEAR = "year"
        private const val COLUMN_RARITY = "rarity"
        private const val COLUMN_VALUE = "value"
        private const val COLUMN_IMAGE_PATH = "imagePath"

        private const val TABLE_TRANSACTION = "coin_transaction"
        private const val COLUMN_TRANSACTION_ID = "id"
        private const val COLUMN_COIN_ID_FK = "coinId"
        private const val COLUMN_QUANTITY = "quantity"
        private const val COLUMN_DATE = "date"
    }

    override fun onCreate(db: SQLiteDatabase) {
        val createCoinTable = ("CREATE TABLE $TABLE_COIN ("
                + "$COLUMN_COIN_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "$COLUMN_YEAR INTEGER,"
                + "$COLUMN_RARITY TEXT,"
                + "$COLUMN_VALUE REAL,"
                + "$COLUMN_IMAGE_PATH TEXT)")
        db.execSQL(createCoinTable)

        val createTransactionTable = ("CREATE TABLE $TABLE_TRANSACTION ("
                + "$COLUMN_TRANSACTION_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "$COLUMN_COIN_ID_FK INTEGER,"
                + "$COLUMN_QUANTITY INTEGER,"
                + "$COLUMN_DATE TEXT,"
                + "FOREIGN KEY($COLUMN_COIN_ID_FK) REFERENCES $TABLE_COIN($COLUMN_COIN_ID))")
        db.execSQL(createTransactionTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        if (oldVersion < 2) {
            db.execSQL("ALTER TABLE $TABLE_COIN ADD COLUMN $COLUMN_IMAGE_PATH TEXT")
        }
    }

    fun addCoin(coin: Coin): Long {
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(COLUMN_YEAR, coin.year)
            put(COLUMN_RARITY, coin.rarity.value)
            put(COLUMN_VALUE, coin.value)
            put(COLUMN_IMAGE_PATH, coin.imagePath)
        }
        val id = db.insert(TABLE_COIN, null, values)
        db.close()
        return id
    }

    fun addTransaction(transaction: CoinTransaction): Long {
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(COLUMN_COIN_ID_FK, transaction.coinId)
            put(COLUMN_QUANTITY, transaction.quantity)
            put(COLUMN_DATE, SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(transaction.date))
        }
        val id = db.insert(TABLE_TRANSACTION, null, values)
        db.close()
        return id
    }

    fun getCoin(year: Int, rarity: Rarity, value: Double): Coin? {
        val db = this.readableDatabase
        val cursor = db.rawQuery(
            "SELECT * FROM $TABLE_COIN WHERE $COLUMN_YEAR = ? AND $COLUMN_RARITY = ? AND $COLUMN_VALUE = ?",
            arrayOf(year.toString(), rarity.value, value.toString())
        )
        var coin: Coin? = null
        if (cursor.moveToFirst()) {
            coin = Coin(
                id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_COIN_ID)),
                year = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_YEAR)),
                rarity = Rarity.entries.find { it.value == cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_RARITY)) }!!,
                value = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_VALUE)),
                imagePath = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_IMAGE_PATH))
            )
        }
        cursor.close()
        return coin
    }

    // Method to get transactions by coin id
    fun getTransactionsByCoinId(coinId: Long): List<CoinTransaction> {
        val db = readableDatabase
        val cursor = db.query(
            TABLE_TRANSACTION,
            arrayOf(COLUMN_TRANSACTION_ID, COLUMN_COIN_ID_FK, COLUMN_QUANTITY, COLUMN_DATE),
            "$COLUMN_COIN_ID_FK = ?",
            arrayOf(coinId.toString()),
            null,
            null,
            "$COLUMN_DATE ASC"
        )
        val transactions = mutableListOf<CoinTransaction>()
        with(cursor) {
            while (moveToNext()) {
                val id = getInt(getColumnIndexOrThrow(COLUMN_TRANSACTION_ID))
                val quantity = getInt(getColumnIndexOrThrow(COLUMN_QUANTITY))
                val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(getString(getColumnIndexOrThrow(COLUMN_DATE)))
                transactions.add(CoinTransaction(id, coinId, quantity, date))
            }
        }
        cursor.close()
        return transactions
    }

    // Method to get all coins
    fun getAllCoins(): List<Coin> {
        val db = readableDatabase
        val cursor = db.query(
            TABLE_COIN,
            arrayOf(COLUMN_COIN_ID, COLUMN_YEAR, COLUMN_RARITY, COLUMN_VALUE, COLUMN_IMAGE_PATH),
            null,
            null,
            null,
            null,
            null
        )
        val coins = mutableListOf<Coin>()
        with(cursor) {
            while (moveToNext()) {
                val id = getLong(getColumnIndexOrThrow(COLUMN_COIN_ID))
                val year = getInt(getColumnIndexOrThrow(COLUMN_YEAR))
                val rarity = Rarity.entries.find { it.value == getString(getColumnIndexOrThrow(COLUMN_RARITY)) }!!
                val value = getDouble(getColumnIndexOrThrow(COLUMN_VALUE))
                val imagePath = getString(getColumnIndexOrThrow(COLUMN_IMAGE_PATH))
                coins.add(Coin(id, year, rarity, value, imagePath))
            }
        }
        cursor.close()
        return coins
    }

    fun getAllCoinTransactions(): List<CoinTransaction> {
        val db = readableDatabase
        val cursor = db.query(
            TABLE_TRANSACTION,
            arrayOf(COLUMN_TRANSACTION_ID, COLUMN_COIN_ID_FK, COLUMN_QUANTITY, COLUMN_DATE),
            null,
            null,
            null,
            null,
            "$COLUMN_DATE ASC"
        )
        val coinTransactions = mutableListOf<CoinTransaction>()
        with(cursor) {
            while (moveToNext()) {
                val id = getInt(getColumnIndexOrThrow(COLUMN_TRANSACTION_ID))
                val coinId = getLong(getColumnIndexOrThrow(COLUMN_COIN_ID_FK))
                val quantity = getInt(getColumnIndexOrThrow(COLUMN_QUANTITY))
                val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(getString(getColumnIndexOrThrow(COLUMN_DATE)))
                coinTransactions.add(CoinTransaction(id, coinId, quantity, date))
            }
        }
        cursor.close()
        return coinTransactions
    }

    // Method to get coin by id
    fun getCoinById(id: Long): Coin? {
        val db = readableDatabase
        val cursor = db.query(
            TABLE_COIN,
            arrayOf(COLUMN_COIN_ID, COLUMN_YEAR, COLUMN_RARITY, COLUMN_VALUE, COLUMN_IMAGE_PATH),
            "$COLUMN_COIN_ID = ?",
            arrayOf(id.toString()),
            null,
            null,
            null
        )
        return if (cursor.moveToFirst()) {
            val year = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_YEAR))
            val rarity = Rarity.entries.find { it.value == cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_RARITY)) }!!
            val value = cursor.getDouble(cursor.getColumnIndexOrThrow(COLUMN_VALUE))
            val imagePath = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_IMAGE_PATH))
            Coin(id, year, rarity, value, imagePath)
        } else {
            null
        }.also {
            cursor.close()
        }
    }

    fun getTotalValueByDate(): List<Pair<Date, Double>> {
        val db = readableDatabase
        val cursor = db.rawQuery("""
        SELECT $COLUMN_DATE, SUM($COLUMN_QUANTITY * $COLUMN_VALUE) as totalValue
        FROM $TABLE_TRANSACTION
        JOIN $TABLE_COIN ON $TABLE_TRANSACTION.$COLUMN_COIN_ID_FK = $TABLE_COIN.$COLUMN_COIN_ID
        GROUP BY $COLUMN_DATE
        ORDER BY $COLUMN_DATE
    """, null)
        val totalValues = mutableListOf<Pair<Date, Double>>()
        with(cursor) {
            while (moveToNext()) {
                val date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(getString(getColumnIndexOrThrow(COLUMN_DATE)))
                val totalValue = getDouble(getColumnIndexOrThrow("totalValue"))
                totalValues.add(date to totalValue)
            }
        }
        cursor.close()
        return totalValues
    }

    fun addPicture(coinId: Long, imagePath: String) {
        val db = writableDatabase
        val contentValues = ContentValues().apply {
            put(COLUMN_IMAGE_PATH, imagePath)
        }
        val selection = "$COLUMN_COIN_ID = ?"
        val selectionArgs = arrayOf(coinId.toString())
        db.update(TABLE_COIN, contentValues, selection, selectionArgs)
    }
}
