package fr.triocode.coincollector.data

import java.util.Date

data class CoinTransaction(
    val id: Int,
    val coinId: Long,
    val quantity: Int,
    var date: Date
)
