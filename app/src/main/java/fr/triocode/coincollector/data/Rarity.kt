package fr.triocode.coincollector.data

enum class Rarity(value: String) {
    COMMON ("Commun"),
    UNCOMMON("Exceptionnel"),
    RARE("Rare"),
    VERY_RARE("Très rare"),
    LEGENDARY("Legendaire");

    val value: String = value
}